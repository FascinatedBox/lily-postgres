#include <stdlib.h>
#include <string.h>

#include "libpq-fe.h"

#include "lily.h"
#include "lily_postgres_bindings.h"

typedef struct lily_postgres_Conn_ {
    LILY_FOREIGN_HEADER
    uint64_t is_open;
    PGconn *conn;
} lily_postgres_Conn;

typedef struct lily_postgres_Cursor_ {
    LILY_FOREIGN_HEADER
    uint64_t column_count;
    uint64_t row_count;
    uint64_t current_row;
    uint64_t is_closed;
    PGresult *pg_result;
} lily_postgres_Cursor;

void destroy_Cursor(lily_postgres_Cursor *);

void destroy_Conn(lily_postgres_Conn *conn_value)
{
    PQfinish(conn_value->conn);
}

void lily_postgres_Conn_literal(lily_state *s)
{
    lily_postgres_Conn *conn_value = ARG_Conn(s, 0);
    PGconn *conn = conn_value->conn;
    lily_string_val *sv = lily_arg_string(s, 1);
    char *raw_input = lily_string_raw(sv);
    uint32_t raw_length = lily_string_length(sv);
    char *buffer = PQescapeLiteral(conn, raw_input, raw_length);
    lily_container_val *result = lily_push_instance(s, ID_QueryString(s), 1);

    lily_push_string(s, buffer);
    SETFS_QueryString__text_(s, result);
    free(buffer);

    lily_return_top(s);
}

void lily_postgres_Conn_query(lily_state *s)
{
    lily_postgres_Conn *conn_value = ARG_Conn(s, 0);
    char *fmt = lily_arg_string_raw(s, 1);
    lily_container_val *vararg_lv = lily_arg_container(s, 2);

    int arg_pos = 0, fmt_index = 0, text_start = 0, text_stop = 0;
    lily_msgbuf *msgbuf = lily_msgbuf_get(s);

    int num_values = lily_con_size(vararg_lv);

    while (1) {
        char ch = fmt[fmt_index];

        if (ch == '?') {
            if (arg_pos == num_values) {
                lily_container_val *variant = lily_push_failure(s);
                lily_push_string(s, "Not enough arguments for format.\n");
                lily_con_set_from_stack(s, variant, 0);
                lily_return_top(s);
                return;
            }

            lily_mb_add_slice(msgbuf, fmt, text_start, text_stop);
            text_start = fmt_index + 1;
            text_stop = text_start;

            lily_value *v = lily_con_get(vararg_lv, arg_pos);
            lily_container_val *con_v = lily_as_container(v);
            lily_value *con_text = GET_QueryString__text_(con_v);

            lily_mb_add(msgbuf, lily_as_string_raw(con_text));
            arg_pos++;
        }
        else {
            if (ch == '\0')
                break;

            text_stop++;
        }

        fmt_index++;
    }

    const char *query_string;

    /* If there are no ?'s in the format string, then it can be used as-is. */
    if (text_start == 0)
        query_string = fmt;
    else {
        lily_mb_add_slice(msgbuf, fmt, text_start, text_stop);
        query_string = lily_mb_raw(msgbuf);
    }

    PGresult *raw_result = PQexec(conn_value->conn, query_string);

    ExecStatusType status = PQresultStatus(raw_result);
    if (status == PGRES_BAD_RESPONSE ||
        status == PGRES_NONFATAL_ERROR ||
        status == PGRES_FATAL_ERROR) {
        lily_container_val *variant = lily_push_failure(s);
        lily_push_string(s, PQerrorMessage(conn_value->conn));
        lily_con_set_from_stack(s, variant, 0);
        lily_return_top(s);
        return;
    }

    lily_container_val *variant = lily_push_success(s);

    lily_postgres_Cursor *res = INIT_Cursor(s);
    res->current_row = 0;
    res->is_closed = 0;
    res->pg_result = raw_result;
    res->row_count = PQntuples(raw_result);
    res->column_count = PQnfields(raw_result);

    lily_con_set_from_stack(s, variant, 0);
    lily_return_top(s);
}

void lily_postgres_Conn_open(lily_state *s)
{
    const char *host = NULL;
    const char *port = NULL;
    const char *dbname = NULL;
    const char *name = NULL;
    const char *pass = NULL;

    switch (lily_arg_count(s)) {
        case 5:
            pass = lily_arg_string_raw(s, 4);
        case 4:
            name = lily_arg_string_raw(s, 3);
        case 3:
            dbname = lily_arg_string_raw(s, 2);
        case 2:
            port = lily_arg_string_raw(s, 1);
        case 1:
            host = lily_arg_string_raw(s, 0);
    }

    PGconn *conn = PQsetdbLogin(host, port, NULL, NULL, dbname, name, pass);
    lily_postgres_Conn *new_val;
    lily_container_val *variant;

    switch (PQstatus(conn)) {
        case CONNECTION_OK:
            variant = lily_push_success(s);

            new_val = INIT_Conn(s);
            new_val->is_open = 1;
            new_val->conn = conn;

            lily_con_set_from_stack(s, variant, 0);
            break;
        default:
            variant = lily_push_failure(s);
            lily_push_string(s, PQerrorMessage(conn));
            lily_con_set_from_stack(s, variant, 0);
            break;
    }

    lily_return_top(s);
}

void close_cursor(lily_postgres_Cursor *result)
{
    if (result->is_closed == 0)
        PQclear(result->pg_result);

    result->is_closed = 1;
}

void destroy_Cursor(lily_postgres_Cursor *r)
{
    close_cursor(r);
}

void lily_postgres_Cursor_close(lily_state *s)
{
    lily_postgres_Cursor *to_close = ARG_Cursor(s, 0);

    close_cursor(to_close);
    to_close->row_count = 0;
}

void lily_postgres_Cursor_each_row(lily_state *s)
{
    lily_postgres_Cursor *boxed_result = ARG_Cursor(s, 0);

    PGresult *raw_result = boxed_result->pg_result;
    if (raw_result == NULL || boxed_result->row_count == 0)
        return;

    lily_call_prepare(s, lily_arg_function(s, 1));

    int row;
    for (row = 0;row < boxed_result->row_count;row++) {
        int num_cols = boxed_result->column_count;
        lily_container_val *lv = lily_push_list(s, num_cols);

        int col;
        for (col = 0;col < num_cols;col++) {
            char *field_text;

            if (PQgetisnull(raw_result, row, col))
                field_text = "(null)";
            else
                field_text = PQgetvalue(raw_result, row, col);

            lily_push_string(s, field_text);
            lily_con_set_from_stack(s, lv, col);
        }

        lily_call(s, 1);
    }
}

void lily_postgres_Cursor_row_count(lily_state *s)
{
    lily_postgres_Cursor *boxed_result = ARG_Cursor(s, 0);

    lily_return_integer(s, boxed_result->current_row);
}

void lily_postgres_QueryString_new(lily_state *s)
{
    lily_RuntimeError(s, "Cannot create instances of QueryString directly.");
}

void lily_postgres_QueryString_text(lily_state *s)
{
    lily_container_val *input = lily_arg_container(s, 0);
    lily_value *result = GET_QueryString__text_(input);

    lily_return_value(s, result);
}

LILY_DECLARE_POSTGRES_CALL_TABLE
