lily-postgres
=============

[View Documentation](https://fascinatedbox.gitlab.io/lily-postgres/postgres/module.postgres.html)

This package provides Lily with a simple wrapper over libpq. You'll need libpq's
development headers, but there are no other requirements. You can install this
using Lily's `garden` via:

`garden install FascinatedBox/lily-postgres`.
